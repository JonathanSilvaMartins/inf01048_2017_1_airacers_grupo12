import controller_template as controller_template, random, numpy


class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)

    sensorsP = [0, 0, 0, 0, 0, 0, 0, 0, 0]  # global variable to keeo the previous state.
    # currently starting it with zeros, let's see what happens
    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################

    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
        features = self.compute_features(self.sensors)

        # in each for loop we get the preference value for each of our actions
        # every feature in our set of features is multiplied by theta(i)
        i = 0
        r = 0
        for feature in features:
            r += parameters[i]*feature
            i += 1
        l = 0
        for feature in features:
            l += parameters[i] * feature
            i += 1
        a = 0
        for feature in features:
            a += parameters[i] * feature
            i += 1
        b = 0
        for feature in features:
            b += parameters[i] * feature
            i += 1
        n = 0
        for feature in features:
            n += parameters[i] * feature
            i += 1

        preference = max(r, l, a, b, n)
        # then we see what action has the greatest preference value and return it's number

        if preference is r:
            return 1
        if preference is l:
            return 2
        if preference is a:
            return 3
        if preference is b:
            return 4
        if preference is n:
            return 5


    def compute_features(self, sensors):
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
         0   track_distance_left: 1-100
         1   track_distance_center: 1-100
         2   track_distance_right: 1-100
         3   on_track: 0 or 1
         4   checkpoint_distance: 0-???
         5   car_velocity: 10-200
         6   enemy_distance: -1 or 0-???
         7   position_angle: -180 to 180
         8   enemy_detected: 0 or 1
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """
        features = []

        # features are normalized in [-1,1] using 2(feature-min)/(max-min) -1
        features.append(1)         # first feature is set to 1 in order to get preferences as foor loops easily
        features.append(sensors[0]/50-1)  # this feature is left distance
        features.append(sensors[1]/50-1)  # this one is center distance
        features.append(sensors[2]/50-1)  # this one is right distance
        features.append(2*sensors[3]-1)   # this one is is_on_track flag
        features.append((sensors[5]-10)/95 - 1)  # this one is car velocity

        diff = self.sensorsP[4] - sensors[4]

        # features.append(2*(diff+20)/40-1)
        """foe_dist = sensors[6] + 1
        foe_angle = sensors[7]
        foe_sight = sensors[8]
        foe_fst_quadr = 0
        foe_scn_quadr = 0
        foe_thr_quadr = 0
        foe_frt_quadr = 0
        if foe_dist > 100:
            foe_dist = 0
        if (foe_angle <= 90) & (foe_angle > 1):
            foe_frt_quadr = 1
        if (foe_angle <= -1) & (foe_angle > -90):
            foe_scn_quadr = 1
        if (foe_angle <= -90) & (foe_angle > -180):
            foe_thr_quadr = 1
        if (foe_angle <= 180) & (foe_angle > 90):
            foe_frt_quadr = 1

        features.append(foe_sight * foe_fst_quadr * foe_dist / 50 - 1)
        features.append(foe_sight * foe_scn_quadr * foe_dist / 50 - 1)
        features.append(foe_sight * foe_thr_quadr * foe_dist / 50 - 1)
        features.append(foe_sight * foe_frt_quadr * foe_dist / 50 - 1)   features que não deram muito certo


        features.append(foe_dist*sensors[8]/50-1)
        features.append((sensors[8]*sensors[7]+180)/180-1)"""""

        self.sensorsP = sensors   ##current state is going to be the next previous state

        return features

    @staticmethod
    def random_neighbour(weights) -> list:
        neighbour = []
        for weight in weights:  # for every theta in our weights list
            epsilon = random.uniform(-0.1, 0.1)  # we generate another theta in a neighbour that's theta
            neighbour.append(weight + epsilon)  # that's basically current theta + a small random
        return neighbour

    @staticmethod
    def decrease_temperature(t) -> int:     # gotta find a good way to decrease this guy over here
        return t - 250

    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE
        
        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """

        t = 2500  # initial temperature   # gotta find a good initial temperature here
        best_value = -9999
        best_weight = []
        while True:
            print(t)
            if t <= 0:   # when temperature reaches zero we return our best set of weights
                return best_weight

            for i in range(1, 40):  # visit 40 neighbours
                reachable_state = self.random_neighbour(weights)
                reachable_value = self.run_episode(reachable_state)
                energy_delta = reachable_value - self.run_episode(weights)  # calculates the energy of the solution

                if reachable_value > best_value:  # save the best neighbour, just to make sure
                    best_value = reachable_value
                    best_weight = reachable_state

                if energy_delta > 0:        # if we've found a better neighbour we go for it
                    weights = reachable_state
                else:
                    prob = numpy.exp(energy_delta/t)    # if it'snot a better one, we go for it with some prob
                    selection = random.uniform(0, 1)
                    if selection <= prob:
                        weights = reachable_state
            t = self.decrease_temperature(t)    # decrease the temperature

        raise NotImplementedError("This Method Must Be Implemented")

