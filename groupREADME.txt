Para treinar pesos � necess�rio criar um .txt com pesos iniciais (sempre uso 0). Com um peso por linha.
O n�mero de pesos � igual a #features*5 (tem que ir em controller1/controller.py em compute_features para ver o n�merp de features) Contando a feature inicial que � a constante 1.

Para treinar, ent�o, chamamos, por exemplo: python AIRacers.py -w weights.txt -t track1 -b parked_bots learn

Para vermos os pesos que o carro aprendeu vamos na pasta params e pegamos o �ltimo arquivo gerado l�, que � nomeado de acordo com o hor�rio em que foi gerado.

Ap�s renomear o arquivo gerado para, por exemplo, wights.txt, testamos ele chamando, por exemplo: python AIRacers.py -w weights.txt -t track1 -b ninja_bot evaluate 